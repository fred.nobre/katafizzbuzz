package com.octo.fizzbuzz;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class FizzBuzzTest {


    @ParameterizedTest
    @ValueSource(ints = {1, 2, 4, 7})
    public void shouldReturnSameNumber(int input) {
        //Given

        //when
        String output = FizzBuzz.generateAnswer(input);


        //then
        assertEquals(String.valueOf(input), output);

    }

    @ParameterizedTest
    @ValueSource(ints = {6, 9, 12})
    public void shouldReturnFizzWhenNumberIsMultiplieOfThree(int input) {
        //Given

        //when
        String output = FizzBuzz.generateAnswer(input);

        //then
        assertEquals("Fizz", output);

    }

    @ParameterizedTest
    @ValueSource(ints = {10, 20})
    public void shouldReturnBuzzWhenNumberIsMultipleOfFive(int input) {
        //Given

        //When
        String output = FizzBuzz.generateAnswer(input);

        //then
        assertEquals("Buzz", output);
    }

    @ParameterizedTest
    @ValueSource(ints = {60})
    public void shouldReturnFizzBuzzWhenNumberIsMultipleOfThreeAndFive(int input){
        //Given

        //When
        String output = FizzBuzz.generateAnswer(input);

        //then
        assertEquals("FizzBuzz", output);
    }

    @ParameterizedTest
    @ValueSource(ints = { 13, 23, 37})
    public void shouldReturnFizzWhenNumberContains3(int input) {
        //Given

        //when
        String output = FizzBuzz.generateAnswer(input);

        //then
        assertEquals("Fizz", output);
    }

    @ParameterizedTest
    @ValueSource(ints = {52})
    public void shouldReturnFizzWhenNumberContains5(int input) {
        //Given

        //when
        String output = FizzBuzz.generateAnswer(input);

        //then
        assertEquals("Buzz", output);
    }


}