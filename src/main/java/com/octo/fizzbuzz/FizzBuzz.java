package com.octo.fizzbuzz;

public class FizzBuzz {

    public static final String FIZZ = "Fizz";
    public static final String BUZZ = "Buzz";

    public static void main(String[] args) {

        int limit = 100;

        if(args.length != 0) {
            limit = Integer.parseInt(args[0]);
        }
        System.out.println(limit);

        for(int i=1; i<=limit; i++) {
            System.out.println(FizzBuzz.generateAnswer(i));
        }
    }


    public static String generateAnswer(int i) {
        String answer = "";

        if (isMultipleofThree(i)) answer +=FIZZ;
        if(containsNumberThree(i)) answer += FIZZ;
        if (isMultipleofFive(i)) answer += BUZZ;
        if(containsNumberFive(i)) answer += BUZZ;

        if(answer == ""){

            return String.valueOf(i);
        }

        return answer;

    }
    private static boolean isMultipleofThree(int i){

        return isMultipleOfNumber(i, 3);
    }

    private static boolean isMultipleofFive(int i){

        return isMultipleOfNumber(i, 5);
    }


    private static boolean isMultipleOfNumber(int i, int divider){
        return i % divider == 0;
    }

    private static boolean containsNumberThree(int i){
        return containsNumber(i,3);
    }

    private static boolean containsNumberFive(int i){
        return containsNumber(i,5);
    }

    private static boolean containsNumber(int i, int number){ return String.valueOf(i).contains(String.valueOf(number));}


}
